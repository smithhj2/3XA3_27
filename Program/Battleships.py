import copy, random


#######################
###                 ###
###   Player move   ###
###                 ###
#######################
def player_move(board):
	
        while(True):
            x,y = get_location()                        
            result = make_move(board,x,y)           

            if result == "hit":
                print "Ship was hit at " + str(x+1) + "," + str(y+1)
                check_sink(board,x,y)
                board[x][y] = '$'              
                if check_win(board):
                    return "WIN"

            elif result == "miss":
                print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
                board[x][y] = "*"               

            elif result == "try again":
                print "Oops! You can't guess there! That coordinate was already hit,\n  or was outside the board range. Please try a different one."	

            if result != "try again":           
                return board



#####################
###               ###
###   Comp move   ###
###               ###
#####################
def computer_move(board):

        while(True):
            #come up with a better way to guess that random numbers

            x = random.randint(1,10)-1
            y = random.randint(1,10)-1


            res = make_move(board,x,y)
		
            if res == "hit":
                print "Hit at " + str(x+1) + "," + str(y+1)
                check_sink(board,x,y)
                board[x][y] = '$'
                if check_win(board):
                    return "WIN"
            elif res == "miss":
                print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
                board[x][y] = "*"

            if res != "try again":
                return board



##############################
###                        ###
###   Placing User Ships   ###
###                        ###
##############################    
def place_player_pieces(board,pieces):

        for piece in pieces.keys():

                length = 0                          
                if piece == "Aircraft Carrier":
                    length = 5
                elif piece == "Battleship":
                    length = 4
                elif piece == "Patrol Boat":
                    length = 2
                else:
                    length = 3

                valid = False
                while(not valid):
                    print_board("p",board)
                    print "Please place your " + piece + " on the board by giving the co-ordinates. \nThis piece is " + str(length) + " squares long"
                    x,y = get_location()
                    orientation = ""
                    orientation = get_orientation()
                    valid = validate(board,pieces[piece],x,y,orientation)
                    if not valid:
                        print "This piece cannot be placed here, please give valid co-ordinates"

                board = place_piece(board,pieces[piece],piece[0],x,y,orientation)

        cont = raw_input("\nAll your pieces have been placed, hit ENTER to continue")

        return board



##############################
###                        ###
###   Placing Comp Ships   ###
###                        ###
##############################
def place_computer_pieces(board,pieces):
    print "\n\n"
    for piece in pieces.keys():

        valid = False
        while(not valid):

            x = random.randint(1,10)-1      
            y = random.randint(1,10)-1      
            o = random.randint(0,1)
             
            if o == 0: 
                orientation = "v"
            else:
                orientation = "h"
            valid = validate(board,pieces[piece],x,y,orientation)
            
        board = place_piece(board,pieces[piece],piece[0],x,y,orientation)
        print "Computer has placed their " + piece
	    
    return board



######################################
###                                ###
###   Placing Ships On The Board   ###
###                                ###
######################################
def place_piece(board,piece,letter,x,y,orientation):

	if orientation == "v":
		for i in range(piece):
			board[x+i][y] = letter
	elif orientation == "h":
		for i in range(piece):
			board[x][y+i] = letter

	return board



#####################################
###                               ###
###   Validating Ship Placement   ###
###                               ###
#####################################
def validate(board,piece,x,y,orientation):

	if orientation == "v" and x+piece > 10:         
		return False
	elif orientation == "h" and y+piece > 10:     
		return False
	else:
		if orientation == "v":     
			for i in range(piece):                  
				if board[x+i][y] != -1:
					return False
		elif orientation == "h": 
			for i in range(piece):                  
				if board[x][y+i] != -1:         
					return False
		
	return True



#######################################
###                                 ###
###   Obtaining Co-ordinate Input   ###
###                                 ###
#######################################
def get_location():
	
	while (True):
		player_input = raw_input("\nPlease enter coordinates as two intergers between 1 and 10 \n  separated by a comma. row,col ")
		try:
			coor = player_input.split(",")                                   
			if len(coor) != 2:
				raise Exception("Invalid entry, please enter TWO coordinates.");

			coor[0] = int(coor[0])-1   
			coor[1] = int(coor[1])-1

			if coor[0] > 9 or coor[0] < 0 or coor[1] > 9 or coor[1] < 0: 
				raise Exception("Invalid entry. Please use values between 1 to 10 only.")

			return coor
		
		except ValueError: 
			print "Invalid entry. Please enter only numeric values for coordinates"
		except Exception as e:
			print e



#######################################
###                                 ###
###   Obtaining Orientation Input   ###
###                                 ###
#######################################
def get_orientation():
        
    while(True):
        player_input = raw_input("\nWould you like to place the piece vertically or horizontally? \nPlease enter either 'v' or 'h' ")
        if player_input == "v" or player_input == "h":
            return player_input
        else:
            print "Invalid input. Please only enter either 'v' or 'h' "



#########################
###                   ###
###   Making a move   ###
###                   ###
#########################
def make_move(board,x,y):

    if board[x][y] == -1:
        return "miss"
    elif board[x][y] == "*" or board[x][y] == "$":
        return "try again"
    else:
        return "hit"



######################
###                ###
###   Check Sink   ###
###                ###
######################
def check_sink(board,x,y):

	if board[x][y] == "A":               
		piece = "Aircraft Carrier"
	elif board[x][y] == "B":
		piece = "Battleship"
	elif board[x][y] == "S":
		piece = "Submarine" 
	elif board[x][y] == "D":
		piece = "Destroyer"
	elif board[x][y] == "P": 
		piece = "Patrol Boat"
	
	board[-1][piece] -= 1       
	if board[-1][piece] == 0:
		print "The " + piece + " has been sunk"



#####################
###               ###
###   Check win   ###
###               ###
#####################
def check_win(board):
	
	for i in range(10):
		for j in range(10):
			if board[i][j] != -1 and board[i][j] != '*' and board[i][j] != '$':
				return False
	return True


                                     
##############################
###                        ###
###   Printing the Board   ###
###                        ###
##############################
def print_board(s,board):

                                        
	player = "Computer"                                     # Find out if you are printing the computer or 
	if s == "p":                                            # user board based on the value of input s
		player = "Player"
	print "\nThe " + player + "'s board look like this: \n"

	print " ",                                              # Print the horizontal numbers, the y's of
	for i in range(10):                                     # the x,y co-ordinates
		print "  " + str(i+1) + "  ",
	print "\n"

	for i in range(10):
		if i != 9:                                      # Print the vertical line number, the x of
			print str(i+1) + "  ",                  # the x,y co-ordinates
		else:
			print str(i+1) + " ",

		for j in range(10):                             # Print the icons in the board spaces
			if board[i][j] == -1:                   # as well as the "|" line segments between the spaces
				print ' ',	
			elif s == "p":
				print board[i][j],
			elif s == "c":
				if board[i][j] == "*" or board[i][j] == "$":
					print board[i][j],
				else:
					print " ",
			if j != 9:
				print " | ",
		print
		
		if i != 9:                                      # print a horizontal line to separate the rows
			print "   ----------------------------------------------------------"
		else: 
			print



########################
###                  ###
###   Update Score   ###
###                  ###
########################
def update_score(winner,score):
        if winner == 'p':
                score[0] += 1
        elif winner == 'c':
                score[1] += 1

        print "\nYour score is: " + str(score[0]) + "\nThe computers score is: " + str(score[1]) + "\n"
        

####################
###              ###
###   Preamble   ###
###              ###
####################
def preamble():
    print "Welcome to the Game of Battleships!\n"
    print "You will be playing against the computer.\n"
    print "You and the computer each have a game board which consists of a 10x10 grid and 5 ship pieces of different sizes."
    print "The goal of the game is to try and guess where your opponent has placed their ships on their board."
    print "After placing your ship pieces on your own board, you will take turns guessing where you think the computer has \n  placed their pieces."
    print "When a guess is made on an empty board space, it is called a miss, misses are marked with '*'."
    print "When a guess is made on a space where a ship lies, it is called a hit, hits are marked with '$'."
    print "When all the spaces covered by a ship piece are hit, that ship has been 'sunk'."
    print "The game is won by sinking all of your opponent's ships.\n"
    print "Follow the prompts printed on screen to play the game.\n"
    print "You will be asked first to place your pieces one by one."
    print "For each, enter the co-ordinate to place the piece."
    print "Then specify if you want it to be placed vertically or horizontally."
    print "The co-ordinate will determine where the top (for vertical) or left (for horizontal) of the piece sits.\n"
    print "Once your pieces and the computers pieces have been placed you will take turns guessing."
    print "You will be asked to enter a pair of co-ordinates just like before to try and guess where you think the computer \n  has placed a piece."
    print "Remember, your goal is to sink all their ships."
    print "\nARE YOU SURE YOU UNDERSTAND HOW TO PLAY?!\n"
    cont = raw_input("Hit enter to play the game")
        

################
###          ###
###   Main   ###
###          ###
################
def main():

        preamble()
    
        pieces = {"Aircraft Carrier":5,     
                  "Battleship":4,          
                  "Submarine":3,
                  "Destroyer":3,
                  "Patrol Boat":2}
        
        score = [0,0]
        
        board = []                          
        for i in range(10):
                row = []
                for j in range(10):
                    row.append(-1)
                board.append(row)

        while(True):
                player_board = copy.deepcopy(board)       
                comp_board = copy.deepcopy(board)           

                player_board.append(copy.deepcopy(pieces))       
                comp_board.append(copy.deepcopy(pieces)) 
                
                player_board = place_player_pieces(player_board, pieces) 
                print_board("p",player_board)
                comp_board = place_computer_pieces(comp_board, pieces)
                print_board("p",comp_board)
                first = True
                first = bool(random.randint(0,1))

                play = True

                while(play):
                        if first:
                            print "\n\nIt's your turn!"
                            raw_input("Hit ENTER to continue")
                            print_board("c", comp_board)            
                            comp_board = player_move(comp_board) 
                            if comp_board == "WIN":             
                                print "You WON! :)"
                                update_score('p',score)
                                break

                            print "\n\nIt's the Computer's turn!"
                            raw_input("Hit ENTER to continue")
                            player_board = computer_move(player_board)
                            print_board("p",player_board)      
                            if player_board == "WIN":		  
                                print "Computer WON! :("
                                update_score('c',score)
                                break

                        else:
                            print "\n\nIt's the Computer's turn!"
                            raw_input("Hit ENTER to continue")
                            player_board = computer_move(player_board) 
                            print_board("p",player_board)      
                            if player_board == "WIN":		   
                                print "Computer WON! :("
                                update_score('c',score)
                                break

                            print "\n\nIt's your turn!"
                            raw_input("Hit ENTER to continue")
                            print_board("c", comp_board)           
                            comp_board = player_move(comp_board)   
                            if comp_board == "WIN":                
                                print "You WON! :)"
                                update_score('p',score)
                                break
                ask = True
                while(ask):
                        again = raw_input("\nWould you like to play again? Enter 'y' for yes and 'n' for no.\n")
                        if again == 'n':
                                quit()
                        
                        elif again == 'y':
                                print "\n===========\n New Game!\n===========\n"
                                ask = False
                        else:
                                print "Invalid input, please enter either 'y' or 'n'"
                        
			
if __name__=="__main__":
    main()
    
