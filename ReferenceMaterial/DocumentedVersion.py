import copy, random

##########################
###                    ###
###   GAME MECHANICS   ###
###                    ###
##########################

# This is a text based version of the board game Battleship
# A player and the computer are each given a board comprised of a 10x10 grid of spaces
# Each board is then populated with five game pieces, the battleships, there are 5 ships in total
#       1) a 2x1 ship called "Patrol Boat
#       2) a 3x1 ship called "Destroyer" 
#       3) a 3x1 ship called "Submarine"
#       4) a 4x1 ship called "Battleship"
#       5) a 5x1 ship called "Aircraft carrier"

# The player and Computer cannot see each others board and take turns guessing
#       where they believe a ship on the oponents board might be
# During each turn the player or computer guesses a co-ordinate x,y on the oponents board

# If the co-ordinate on the oponents board is empty then that turn was a "miss"
# If there was a ship covering that space then the turn was a "Hit"
# If all the spaces a ship covers are "hit" by the oponent, then that ship is "sunk"

# A player wins when all the ships on the opponents board have been sunk
# A player looses if all of their ships are sunk



##############################
###                        ###
###   Printing the Board   ###
###                        ###
##############################

# The board for each player is a grid of 10x10 spaces
# An empty space denotes a co-ordinate with no ship or guess
# A space with "$" denotes where you have hit a ship on your oponents board
# A space with "*" denotes where you have missed on your oponents board
# A space with "A" "B" "D" "P" "S" denotes where a ship of that name is on your board

def print_board(s,board):

                                        
	player = "Computer"                                     # Find out if you are printing the computer or 
	if s == "u":                                            # user board based on the value of input s
		player = "User"
	print "The " + player + "'s board look like this: \n"

	print " ",                                              # Print the horizontal numbers, the y's of
	for i in range(10):                                     # the x,y co-ordinates
		print "  " + str(i+1) + "  ",
	print "\n"

	for i in range(10):
		if i != 9:                                      # Print the vertical line number, the x of
			print str(i+1) + "  ",                  # the x,y co-ordinates
		else:
			print str(i+1) + " ",

		for j in range(10):                             # Print the icons in the board spaces
			if board[i][j] == -1:                   # as well as the "|" line segments between the spaces
				print ' ',	
			elif s == "u":
				print board[i][j],
			elif s == "c":
				if board[i][j] == "*" or board[i][j] == "$":
					print board[i][j],
				else:
					print " ",
			if j != 9:
				print " | ",
		print
		
		if i != 9:                                      # print a horizontal line to separate the rows
			print "   ----------------------------------------------------------"
		else: 
			print 




##############################
###                        ###
###   Placing User Ships   ###
###                        ###
##############################

# Before starting the game the human player must decide where to place their ships
# Each ship is placed using an input co-ordinate which determines the left-most
#       or top-most space the ship will start in. Then the user decides to place
#       it either vertically or horizontally.
# If the ship is placed horizontally it will run it's given length rightwards from the co-ordinate
# If the ship is placed vertically it will run it's given length downwards from the co-ordinate
# If the user chooses a co-ordinate and orientation that does not fit the program will ask them to try again
# Placement is done once all five ships have been placed on the board


def user_place_ships(board,ships):

	for ship in ships.keys():

		valid = False                                           # Obtain co-ordinates and validate them
		while(not valid):                               

			print_board("u",board)
			print "Placing a/an " + ship
			x,y = get_coor()                                # Obtain input co-ordinates
			ori = v_or_h()                                  # Obtain input orientation
			valid = validate(board,ships[ship],x,y,ori)     # Check validity of placement
			if not valid:
				print "Cannot place a ship there.\nPlease take a look at the board and try again."
				raw_input("Hit ENTER to continue")

		board = place_ship(board,ships[ship],ship[0],ori,x,y)   # Place ship
		#print_board("u",board)
		
	raw_input("Done placing user ships. Hit ENTER to continue")
	return board



##############################
###                        ###
###   Placing Comp Ships   ###
###                        ###
##############################

# Before starting the game the computer must decide where to place their ships
# Computer is not given any strategical or logical placement techniques
#       it simply computes random placements and orientations and
#       places the ships there if the placement is valid, if not it tries again
# Placement is done once all five ships have been placed on the board

def computer_place_ships(board,ships):

	for ship in ships.keys():
	
		valid = False                           # Pick a random location and orientation, and check validity
		while(not valid):

			x = random.randint(1,10)-1      # Pick a random row
			y = random.randint(1,10)-1      # Pick a random column
			o = random.randint(0,1)         # Pick a random orientation
			if o == 0: 
				ori = "v"
			else:
				ori = "h"
			valid = validate(board,ships[ship],x,y,ori)     # Check validity of placement

		print "Computer placing a/an " + ship                   # Place the ship
		board = place_ship(board,ships[ship],ship[0],ori,x,y)
	
	return board



######################################
###                                ###
###   Placing Ships On The Board   ###
###                                ###
######################################

# Given the co-ordinates, orientation, board and ship; place that ship in that formation on that board
# Changes either the row or column of the board to the symbol for that ship

def place_ship(board,ship,s,ori,x,y):

	if ori == "v":                          # Place ship based on orientation v or h
		for i in range(ship):
			board[x+i][y] = s
	elif ori == "h":
		for i in range(ship):
			board[x][y+i] = s

	return board



#####################################
###                               ###
###   Validating Ship Placement   ###
###                               ###
#####################################

# Since the board has specific dimensions, and since ships may not overlap,
#       ships cannot be placed just anywhere
# Ships cannot be placed in spaces which are already occupied
#       i.e. if any space the ship will cover is not empty(-1) the placement is not valid
# Ships cannot be placed only partially on the board
#       i.e. if it's base co-ordinate + ship length > 10 the placement is not valid

def validate(board,ship,x,y,ori):

	
	if ori == "v" and x+ship > 10:          # Check if the ship fits vertically on the board 
		return False
	elif ori == "h" and y+ship > 10:        # Check if the ship fits horizontally on the board 
		return False
	else:
		if ori == "v":                                  # Check if all cells the ship will cover are empty (-1)
			for i in range(ship):                   #       for a vertical ship
				if board[x+i][y] != -1:
					return False
		elif ori == "h":                                # Check if all cells the ship will cover are empty (-1)
			for i in range(ship):                   #       for a horizontal ship
				if board[x][y+i] != -1:         
					return False
		
	return True



#######################################
###                                 ###
###   Obtaining Orientation Input   ###
###                                 ###
#######################################

# When placing the ships before starting the game the player decides whether the ship
#       they place should run vertically or horizontally
# User input should be a single character, either "v" or "h"

def v_or_h():
        
	while(True):
		user_input = raw_input("vertical or horizontal (v,h) ? ")
		if user_input == "v" or user_input == "h":
			return user_input
		else:
			print "Invalid input. Please only enter v or h"


#######################################
###                                 ###
###   Obtaining Co-ordinate Input   ###
###                                 ###
#######################################

# When placing the ships before starting the game the player decides which  
#      co-ordinate they want the ship to begin at
# The ship extends rightward or downward from this co-ordinate
# Input should be two positive integers <=10 separated by a ","  ex: 4,6

def get_coor():
	
	while (True):
		user_input = raw_input("Please enter coordinates (row,col) ? ")
		try:
			coor = user_input.split(",")                                    # Obtain inputs, split by the comma to separate the values
			if len(coor) != 2:
				raise Exception("Invalid entry, too few/many coordinates.");

			coor[0] = int(coor[0])-1                                        # Check the values are integers
			coor[1] = int(coor[1])-1

			if coor[0] > 9 or coor[0] < 0 or coor[1] > 9 or coor[1] < 0:    #Check that the values are in the correct range (0..9)
				raise Exception("Invalid entry. Please use values between 1 to 10 only.")

			return coor
		
		except ValueError:                                                      # Raise exception if inputs were invalid
			print "Invalid entry. Please enter only numeric values for coordinates"
		except Exception as e:
			print e


#########################
###                   ###
###   Making a move   ###
###                   ###
#########################

# Moves are made by checking the value of the opponent board at guess x,y
# Move either results in a hit($) or miss(*)
# If the co-ordinate has been previously guessed, the player may guess again

def make_move(board,x,y):
	
	if board[x][y] == -1:                                   # Based on the symbol / value at the guessed space
		return "miss"                                   #       return the appropriate response
	elif board[x][y] == '*' or board[x][y] == '$':
		return "try again"
	else:
		return "hit"


#####################
###               ###
###   User move   ###
###               ###
#####################

# Obtains the user guess of co-ordinate x,y and attempts to make a move there
# Depending on the result of making the move the user move can result in a miss or a hit
# A hit cna also result in sinking a ship
# Sinking a ship may result in winning the game if it is the final ship to sink

def user_move(board):
	
	while(True):
		x,y = get_coor()                                # Obtain guess and attempt to make the move
		res = make_move(board,x,y)                      #       checking the result and printing information
		if res == "hit":
			print "Hit at " + str(x+1) + "," + str(y+1)
			check_sink(board,x,y)           
			board[x][y] = '$'
			if check_win(board):
				return "WIN"
		elif res == "miss":
			print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
			board[x][y] = "*"
		elif res == "try again":
			print "Sorry, that coordinate was already hit. Please try again"	

		if res != "try again":
			return board



#####################
###               ###
###   Comp move   ###
###               ###
#####################

# Similar to User Move except the guess is simply a randomly generated number

def computer_move(board):
	
	while(True):
		x = random.randint(1,10)-1
		y = random.randint(1,10)-1
		res = make_move(board,x,y)
		if res == "hit":
			print "Hit at " + str(x+1) + "," + str(y+1)
			check_sink(board,x,y)
			board[x][y] = '$'
			if check_win(board):
				return "WIN"
		elif res == "miss":
			print "Sorry, " + str(x+1) + "," + str(y+1) + " is a miss."
			board[x][y] = "*"

		if res != "try again":
			
			return board



######################
###                ###
###   Check Sink   ###
###                ###
######################

# If a ship has been hit, check if each all that ship spaces have been hit
# If every space of a given ship has been hit then that ship has sunk
#       Print this information to the user

def check_sink(board,x,y):

	if board[x][y] == "A":                  # Obtain ship name
		ship = "Aircraft Carrier"
	elif board[x][y] == "B":
		ship = "Battleship"
	elif board[x][y] == "S":
		ship = "Submarine" 
	elif board[x][y] == "D":
		ship = "Destroyer"
	elif board[x][y] == "P": 
		ship = "Patrol Boat"
	
	board[-1][ship] -= 1                    # Mark the guessed cell as a hit
	if board[-1][ship] == 0:                # Check if all spaces are hits, print if true
		print ship + " Sunk"


		
#####################
###               ###
###   Check win   ###
###               ###
#####################

# If a ship has been hit, check if each all ship spaces have been hit
# If every space of all ships have been hit then the game is won
# Ship spaces that are not hit are marked with the ship's letter
#       if there are no more character spaces left (Only empty, * and $)
#       then all ships are sunk and the game is over

def check_win(board):
	
	for i in range(10):
		for j in range(10):
			if board[i][j] != -1 and board[i][j] != '*' and board[i][j] != '$':
				return False
	return True



################
###          ###
###   Main   ###
###          ###
################

# The main method sets up much of the game logistics
#       Sets up the shipds, game board, and ship placement
# Contains the main game loop which quits when the game is won
#       Loop controls user and computer moves, printing the boards
#       and checking for wins

def main():

	ships = {"Aircraft Carrier":5,                          # Define the types of ships
		     "Battleship":4,
 		     "Submarine":3,
		     "Destroyer":3,
		     "Patrol Boat":2}

	board = []                                              # Setup blank 10x10 board
	for i in range(10):
		board_row = []
		for j in range(10):
			board_row.append(-1)
		board.append(board_row)

	user_board = copy.deepcopy(board)                       # Setup user and computer boards
	comp_board = copy.deepcopy(board)

	user_board.append(copy.deepcopy(ships))                 # Add ships as last element in the array
	comp_board.append(copy.deepcopy(ships))

	user_board = user_place_ships(user_board,ships)         # Ship placement
	comp_board = computer_place_ships(comp_board,ships)

	
	while(1):                                               # Game main loop

		print_board("c",comp_board)                     # User move
		comp_board = user_move(comp_board)

		if comp_board == "WIN":                         # Check if user won
			print "User WON! :)"
			quit()
			
		print_board("c",comp_board)                     # Display current computer board
		raw_input("To end user turn hit ENTER")

		user_board = computer_move(user_board)          # Computer move
		
		if user_board == "WIN":		                # Check if computer move
			print "Computer WON! :("
			quit()
		
		print_board("u",user_board)                     # Display user board
		raw_input("To end computer turn hit ENTER")
	
if __name__=="__main__":
	main()
